# docker-images
Personal docker repository by Timmy Wong (thcathy)

https://hub.docker.com/u/thcathy/

# What is included in my repository

### ESL-MYSQL
out-of-the-box MySQL Docker image that *just works* on Mac OS X.
Including write support for mounted volumes.

```bash
docker run -d --name <container name>  \
-e MYSQL_ROOT_PASSWORD=<password> \
-e MYSQL_DATABASE=<default database name:esl> \
-e MYSQL_USER=<default user name:esl> \
-e MYSQL_PASSWORD=<password> \
-p <host's port>:3306 \
-v <host's volume>:/var/lib/mysql \
thcathy/esl-mysql
```

### ESL
Tomcat server running on JRE 8 bundled the web application ESL (www.funfunspell.com). A free English exercise website.

*Pre-requisites*: Started mysql image - ESL-MYSQL

```bash
docker run -d --name <container name> \
-e MYSQL_HOST=<mysql url> \
-e MYSQL_USER=<mysql username>
-e MYSQL_PASSWORD=<mysql password> \
--link <mysql container name> \
-v <host's volume>:/usr/local/tomcat/logs \
thcathy/esl
```

### Squote
A personal java web application running on JRE 8.

*Pre-requisites*: [Started mongodb](https://hub.docker.com/_/mongo/)

```bash
docker run --name <container name> \
-e MONGO_HOST=<mongodb url> \
-p <host's port>:8090 \
--link <mongodb container name> \
thcathy/squote
```

### Squote-web
A nginx proxy server hosting the squote interface, which built by angular 2, and forward request to ESL and Squote

*Pre-requisites*: Started ESL and Squote

```bash
docker run -d --name <container name> \
-p <host's port>:80 \
--link <squote container name> --link <esl container name> \
thcathy/nginx-proxy
```